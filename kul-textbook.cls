% hyle-textbook.cls
% Textbook class based on template by Jan Heylen

\ProvidesClass{kul-textbook}[2017/12/06 version 0.1]
\NeedsTeXFormat{LaTeX2e}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

\ProcessOptions \relax

\LoadClass[a4paper,12pt,twoside,openright,final]{memoir}

% font encoding

\RequirePackage[utf8]{inputenc}    % utf8 support       %!!!!!!!!!!!!!!!!!!!!
\RequirePackage[T1]{fontenc}       % code for pdf file  %!!!!!!!!!!!!!!!!!!!!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add the subliminal stuff

\RequirePackage{microtype}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%language packages

\RequirePackage[english]{babel}
\RequirePackage{csquotes}
\RequirePackage{hyphenat}  %hyphenation of words that contain hyphens

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%math packages

\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{amsthm}

%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{libertine} %font
\RequirePackage[libertine]{newtxmath} %math font

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% memoir chapter and page style

\setsecnumdepth{subsubsection}
\pagestyle{ruled}
\aliaspagestyle{part}{empty}  %no page number on part title page


\RequirePackage{xcolor,calc}
\newsavebox{\ChpNumBox}
\definecolor{ChapBlue}{rgb}{0.32,0.74,0.93} %{rgb}{0.00,0.65,0.65}
\makeatletter
\newcommand
*
{\thickhrulefill}{%
\leavevmode\leaders\hrule height 1\p@ \hfill \kern \z@}
\newcommand
*
\BuildChpNum[2]{%
\begin{tabular}[t]{@{}c@{}}
\makebox[0pt][c]{#1\strut}  \\[.5ex]
\colorbox{ChapBlue}{%
\rule[-10em]{0pt}{0pt}%
\rule{1ex}{0pt}\color{black}#2\strut
\rule{1ex}{0pt}}%
\end{tabular}}
\makechapterstyle{BlueBox}{%
\renewcommand{\chapnamefont}{\large\scshape}
\renewcommand{\chapnumfont}{\Huge\bfseries}
\renewcommand{\chaptitlefont}{\raggedright\Huge\bfseries}
\setlength{\beforechapskip}{20pt}
\setlength{\midchapskip}{26pt}
\setlength{\afterchapskip}{40pt}
\renewcommand{\printchaptername}{}
\renewcommand{\chapternamenum}{}
\renewcommand{\printchapternum}{%
\sbox{\ChpNumBox}{%
\BuildChpNum{\chapnamefont\@chapapp}%
{\chapnumfont\thechapter}}}
\renewcommand{\printchapternonum}{%
\sbox{\ChpNumBox}{%
\BuildChpNum{\chapnamefont\vphantom{\@chapapp}}%
{\chapnumfont\hphantom{\thechapter}}}}
\renewcommand{\afterchapternum}{}
\renewcommand{\printchaptertitle}[1]{%
\usebox{\ChpNumBox}\hfill
\parbox[t]{\hsize-\wd\ChpNumBox-1em}{%
\vspace{\midchapskip}%
\thickhrulefill\par
\chaptitlefont ##1\par}}%
}
\chapterstyle{BlueBox}

%%%%%%%%%%%%%%%%%%%%%%%%%%


\RequirePackage{graphicx}

%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{setspace}
\OnehalfSpacing

%%%%%%%%%%%%%%%%%%%%%%

%figures and diagrams

\RequirePackage{tikz}
\usetikzlibrary{arrows,shapes,positioning,shadows,trees,calc}

\RequirePackage{float}

%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage{shorttoc}

\RequirePackage{diagbox}

\RequirePackage{enumitem}   %%%% resuming enumeration

%%%%%%%%%%%%%%%%%%%
%advanced theorem, definitions, etc options

\RequirePackage{thmtools}

\theoremstyle{plain}
\newtheorem{stelling}{Stelling}[chapter]
 \newtheorem{axioma}{Axioma}
 \newtheorem{theorie}{Theorie}[chapter]
 %\newtheorem{lemma}[stelling]{Lemma}
  \newtheorem{gevolg}{Gevolg}
\theoremstyle{definition}
 \newtheorem{definitie}{Definitie}[chapter]
 \newtheorem{voorbeeld}{Voorbeeld}[chapter]
 \newtheorem{tegenvoorbeeld}{Tegenvoorbeeld}
  \newtheorem{charac}{Karakterisering}[chapter]
  \newtheorem{incharac}{Initi\"{e}le karakterisering}[chapter]

%%%%%%%%%%%%%%%%%%%%%%%%

%creating lists

\RequirePackage[refpage,intoc]{nomencl}
\makenomenclature
\makeindex

\renewcommand{\nomname}{Lijst van symbolen}
\renewcommand{\pagedeclaration}{\quad}

%\RequirePackage[nottoc]{tocbibind}

%\RequirePackage{qtree}

%%%%%%%%%%%%%%%%%%%%%%%%%%%

%handling hyperlinks

\RequirePackage[
	%backref = page,				% so the bibliography contains backreferences to page numbers
	linktoc = all,
%	colorlinks = true,				% kleur weg voor printen
%	linkcolor = blue,  				% kleur weg voor printen
%	urlcolor = blue,				% kleur weg voor printen
%	anchorcolor = blue,			% kleur weg voor printen
%	citecolor = blue				% kleur weg voor printen
	hidelinks
	]{hyperref}
\urlstyle{same}

%%%%%%%%%%%%%%%%%%%%

%bibliography management

\RequirePackage[authordate-trad,backend=biber,hyperref=true,
            backref=true,noibid,bibencoding=utf8]{biblatex-chicago}

\makeatletter
\AtBeginDocument{%
  \toggletrue{blx@useprefix}
  \renewcommand*{\mkbibnameprefix}[1]{\MakeCapital{#1}}}% this is the relevant line of code
\AtBeginBibliography{%
  \togglefalse{blx@useprefix}
  \renewcommand*{\mkbibnameprefix}[1]{#1}}% this sets the name prefix back

\makeatother
